# LM backend API

Project configuration

1. install python3.6
2. install virtualenv
3. activate virtualenv

4. install all the requirement from requirement.txt
pip install -r requirement.txt

5. database is postgre
setup db credential in configs/db.py

### Migarate
```bash
$ flask db stamp head
$ flask db migrate
$ flask db upgrade
```
    