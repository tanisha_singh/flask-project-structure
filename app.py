# import newrelic.agent
# from application.configs.environ import app_environment
# newrelic.agent.initialize('newrelic.ini',app_environment.lower())



from application import  create_app

application = create_app()


if __name__ == "__main__":
    application.run( debug=True)
