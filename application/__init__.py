from application.utilities.flask import APIError, APIFlask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate
# import sentry_sdk
from flask import Flask 
from application.configs import Config


db = SQLAlchemy()
def create_app(test_config=None):
    app = APIFlask(__name__, instance_relative_config=True)
    # app configuration
    app.config.from_object(Config)
    

# initialzie db and migrate
    db.init_app(app)
    Migrate(app, db, compare_type=True)

    with app.app_context():

        from application.routes import test
        from application.routes import about
        from application.routes import all_users
        from application.routes import auth
        from application.routes import user_details
        from application.routes import change_password

        app.register_blueprint(test.test_bp)
        app.register_blueprint(about.about_bp)
        app.register_blueprint(all_users.all_users_bp)
        app.register_blueprint(user_details.user_deatils_bp)
        app.register_blueprint(auth.auth_bp)
        app.register_blueprint(change_password.change_password_bp)
        return app
