from application.utilities.flask import APIResponse
from flask import Blueprint
from flask import Response
from application.configs.jwt import JWTConfig
from flask import Flask, request, jsonify, make_response,render_template
from flask_sqlalchemy import SQLAlchemy
from application.models.user import User

import jwt
import flask
from functools import wraps

user_deatils_bp = Blueprint("user_details", __name__)

# decorator for verifying the JWT
def token_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		token = None
		# print("sfnaf")
		# print(request.headers.get('authorization'))
		# jwt is passed in the request header
		# if 'authorization' in request.headers:
			
		token = request.headers.get('authorization').split()
		# return 401 if token is not passed
		if not token:
			return jsonify({'message' : 'Token is missing !!'}), 401

		try:
			# decoding the payload to fetch the stored details
			# print(token)
			algorithm = "HS256"
			data = jwt.decode(token[1], "symbtech",algorithms=[algorithm],verify=True)
			
			# print(data)
			current_user = User.query\
				.filter_by(email = data['email']).all()
				
		except:
			return jsonify({
				'message' : 'Token is invalid !!'
			}), 401
		# returns the current logged in users contex to the routes
		return f(current_user, *args, **kwargs)

	return decorated

# test route 1
@user_deatils_bp.route('/user_details', methods =['GET'])
@token_required
def user_deatils(current_user):
	# querying the database
	# for all the entries in it
	current_user = str(current_user[0])
	current_user = current_user[:len(current_user)-1]
	current_user = current_user.split()
	print(current_user[0])
	

	users_data = User.query\
				.filter_by(email = current_user[1]).all()
	
	# converting the query objects
	# to list of jsons
	output = []
	for user in users_data:
		# appending the user data json
		# to the response list
		output.append({'name' : user.name,'email' : user.email, 'phone_number':user.phone_number})

	return jsonify({'users': output})

    

