from application.utilities.flask import APIResponse
from flask import Blueprint
from flask import Response
from application.configs.jwt import JWTConfig
from application.models.user import User
from flask import Flask, request, jsonify, make_response,render_template

all_users_bp = Blueprint("all_users", __name__)

# test route 1
# route to print all the users 
@all_users_bp.route('/user',methods = ['GET'])
def get_all():
	users = User.query.all()
	output = []
	for user in users:
		# appending the user data json
		# to the response list
		output.append({'name' : user.name,'email' : user.email,'password': user.password, 'phone_number':user.phone_number})

	return jsonify({'users': output})

    

