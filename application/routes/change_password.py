from application.utilities.flask import APIResponse
from flask import Blueprint
from flask import Response
from application.configs.jwt import JWTConfig
from flask import Flask, request, jsonify, make_response,render_template
from flask_sqlalchemy import SQLAlchemy
from application.models.user import User
from application import db

import jwt
import flask
from functools import wraps

change_password_bp = Blueprint("change_password", __name__)

# test route 1
# decorator for verifying the JWT
def token_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		token = None
		# print("sfnaf")
		# print(request.headers.get('authorization'))
		# jwt is passed in the request header
		# if 'authorization' in request.headers:
			
		token = request.headers.get('authorization').split()
		# return 401 if token is not passed
		if not token:
			return jsonify({'message' : 'Token is missing !!'}), 401

		try:
			# decoding the payload to fetch the stored details
			# print(token)
			algorithm = "HS256"
			data = jwt.decode(token[1], "symbtech",algorithms=[algorithm],verify=True)
			
			# print(data)
			current_user = User.query\
				.filter_by(email = data['email']).all()
				
		except:
			return jsonify({
				'message' : 'Token is invalid !!'
			}), 401
		# returns the current logged in users contex to the routes
		return f(current_user, *args, **kwargs)

	return decorated
# route to change user password with verifying user 
@change_password_bp.route('/change_password', methods =['POST'])
@token_required
def change_password(current_user):
	# querying the database
	# for all the entries in it
	current_user = str(current_user[0])
	current_user = current_user[:len(current_user)-1]
	current_user = current_user.split()
	print(current_user[0])
	
	new_password = request.json.get('new_password')
   

    

	users_data = User.query\
				.filter_by(email = current_user[1]).all()
	
	# converting the query objects
	if users_data:
		users_data[0].password = new_password
		db.session.commit()
		return flask.jsonify({"name": users_data[0].name, "email": users_data[0].email, "password": users_data[0].password, "phone_number": users_data[0].phone_number})
	else:
		return "please login in"

    

